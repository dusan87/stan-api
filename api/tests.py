import json
from django.test import TestCase
from django.shortcuts import reverse
from api.forms import JsonDataForm
from api.views import APIView
# Create your tests here.


class APITestCase(TestCase):

    def test_wrong_json_format_form_returns_errors(self):
        form = JsonDataForm({'data': 'payload: []'})

        self.assertFalse(form.is_valid())
        self.assertIn('data', form.errors)
        self.assertIn('Could not decode request', form.errors['data'][0])

    def test_filter_data_should_return_only_objects_with_episode_count_gte_zero_and_drm_true(self):
        data = {
            'payload': [
                {
                    'drm': True,
                    'episodeCount': 3,
                    'slug': 'show/16kidsandcounting',
                    'title': '16 Kids and Counting',
                    'image': {
                        'showImage': 'http://image.jpg'
                    }
                },
                {
                    'drm': False,
                    'episodeCount': 10,
                    'slug': 'show/16kidsandcounting',
                    'title': '17 Kids and Counting',
                    'image': {'showImage': 'http://image1.jpg'}
                },
                {
                    'title': '18 Kids and Counting'
                },
                {
                    'slug': 'show/seapatrol',
                    'title': 'Sea Patrol',
                    'tvChannel': 'Channel 9'
                }
            ]
        }

        filtered_data = APIView.filter_payload(data)

        self.assertEqual(len(filtered_data), 1)
        self.assertEqual(filtered_data[0]['title'], '16 Kids and Counting')
        self.assertEqual(filtered_data[0]['slug'], 'show/16kidsandcounting')
        self.assertEqual(filtered_data[0]['image'], 'http://image.jpg')


class APIIntegrationTestCase(TestCase):

    def setUp(self):
        self.url = reverse('api-view')

    def test_wrong_json_format_returns_error(self):
        response = self.client.post(self.url, 'payload: []', content_type='application/json')

        self.assertEqual(response.status_code, 400)
        response = json.loads(response.content)

        self.assertIn('error', response)
        self.assertIn('Could not decode request', response['error'])

    def test_post_data_should_return_filtered_data_by_episode_count_gte_zero_and_drm_true(self):

        data = {
            'payload': [
                {
                    'drm': True,
                    'episodeCount': 3,
                    'slug': 'show/16kidsandcounting',
                    'title': '16 Kids and Counting',
                    'image': {
                        'showImage': 'http://image.jpg'
                    }
                },
                {
                    'drm': False,
                    'episodeCount': 10,
                    'slug': 'show/16kidsandcounting',
                    'title': '17 Kids and Counting',
                    'image': {'showImage': 'http://image1.jpg'}
                },
                {
                    'title': '18 Kids and Counting'
                },
                {
                    'slug': 'show/seapatrol',
                    'title': 'Sea Patrol',
                    'tvChannel': 'Channel 9'
                }
            ]
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)

        self.assertIn('response', response_data)
        filtered_shows = response_data['response']

        self.assertEqual(len(filtered_shows), 1)
        self.assertEqual(filtered_shows[0]['title'], '16 Kids and Counting')
        self.assertEqual(filtered_shows[0]['slug'], 'show/16kidsandcounting')
        self.assertEqual(filtered_shows[0]['image'], 'http://image.jpg')

    def test_post_data_should_return_filtered_show_with_none_image_value(self):
        data = {
            'payload': [
                {
                    'drm': True,
                    'episodeCount': 3,
                    'slug': 'show/16kidsandcounting',
                    'title': '16 Kids and Counting',
                    'image': {}
                }
            ]
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)

        self.assertIn('response', response_data)
        filtered_shows = response_data['response']

        self.assertEqual(len(filtered_shows), 1)
        self.assertEqual(filtered_shows[0]['image'], None)

    def test_post_data_should_return_filtered_show_with_none_slug_value(self):
        data = {
            'payload': [
                {
                    'drm': True,
                    'episodeCount': 3,
                    'title': '16 Kids and Counting',
                    'image': {'showImage': 'http://image1.jpg'}
                }
            ]
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)

        self.assertIn('response', response_data)
        filtered_shows = response_data['response']

        self.assertEqual(len(filtered_shows), 1)
        self.assertEqual(filtered_shows[0]['slug'], None)

    def test_post_data_should_return_filtered_show_with_none_title_value(self):
        data = {
            'payload': [
                {
                    'drm': True,
                    'episodeCount': 3,
                    'slug': 'show/16kidsandcounting',
                    'image': {'showImage': 'http://image1.jpg'}
                }
            ]
        }

        response = self.client.post(self.url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)

        self.assertIn('response', response_data)
        filtered_shows = response_data['response']

        self.assertEqual(len(filtered_shows), 1)
        self.assertEqual(filtered_shows[0]['title'], None)
