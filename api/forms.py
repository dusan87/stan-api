import json
from django import forms


class JsonDataForm(forms.Form):
    data = forms.CharField()

    def clean_data(self):
        try:
            data = json.loads(self.cleaned_data['data'])
        except ValueError:
            raise forms.ValidationError('Could not decode request: JSON parsing failed')

        return data
