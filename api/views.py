from django.views.generic import View
from django.http.response import JsonResponse
from api.forms import JsonDataForm
# Create your views here.


class APIView(View):

    @staticmethod
    def filter_payload(data):
        """
        It filters out all objects within data which `episodeCount > 0` and `drm is True`.
        If an object passes the condition but not have any of
        following attributes "image.showImage, title or slug"
        we populate these values as None.

        :param data: ["payload": {...}, {...}]
        :return: [{"title": ..., "image": ..., "slug": ...}]
        """
        shows = []

        for show_obj in data['payload']:
            if show_obj.get('episodeCount', 0) > 0 and show_obj.get('drm') is True:
                image = show_obj.get('image', {})
                shows.append({
                    'image': image.get('showImage'),
                    'slug': show_obj.get('slug'),
                    'title': show_obj.get('title')
                })

        return shows

    def post(self, request, *args, **kwargs):
        form = JsonDataForm({'data': request.body})

        if not form.is_valid():
            return JsonResponse({'error': form.errors['data'][0]}, status=400)

        data = form.cleaned_data['data']
        shows = self.filter_payload(data=data)

        return JsonResponse({'response': shows})
